/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.hiik.studentcore.list;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.hiik.studentcore.Person;

/**
 *
 * @author developer
 */
public class PersonListTest {
    
    Person person1 = new Person();
    Person person2 = new Person();
    Person person3 = new Person();
    Person person4 = new Person();
    
    List<Person> personList = new ArrayList();
    Set <Person> personSet = new HashSet<>();
    Map <Long, Person> personMap = new HashMap<>();
    
    
    
    
    @Test
    public void personList ()
    {
        System.out.println("\nФормирование списка  List");
        person1.id = 1L;
        person1.fistName = "Иван";
        person1.middleName = "Иванoвич";
        person1.sureName = "Иванoв";
        person1.birthDate = LocalDate.of(1992, Month.MARCH, 1);
        
        person2.id = 1L;
        person2.fistName = "Иван";
        person2.middleName = "Иванoвич";
        person2.sureName = "Иванoв";
        person2.birthDate = LocalDate.of(1992, Month.MARCH, 1);
        
       
        
        personList.add(person1);
        personList.add(person2);
        
        System.out.println("Список: "+personList);
    
    }
    
      @Test
    public void personSet ()
    {
        System.out.println("\nФормирование набора Set");
        
        person1.id = 1L;
        person1.fistName = "Иван";
        person1.middleName = "Иванoвич";
        person1.sureName = "Иванoв";
        person1.birthDate = LocalDate.of(1992, Month.MARCH, 1);
        
        person2.id = 2L;
        person2.fistName = "Иван";
        person2.middleName = "Иванoвич";
        person2.sureName = "Иванoв";
        person2.birthDate = LocalDate.of(1992, Month.MARCH, 2);
        
        person3.id = 3L;
        person3.fistName = "Иван";
        person3.middleName = "Иванoвич";
        person3.sureName = "Иванoв";
        person3.birthDate = LocalDate.of(1992, Month.DECEMBER, 15);
        
        person4.id = 4L;
        person4.fistName = "Петр";
        person4.middleName = "Иванoвич";
        person4.sureName = "Иванoв";
        person4.birthDate = LocalDate.of(1992, Month.DECEMBER, 15);
        
        
        
        personSet.add(person1);
        personSet.add(person2);
        personSet.add(person3);
        personSet.add(person4);
        
        System.out.println("Набор: "+personSet);
    
    }
    
       @Test
    public void personMap ()
    {
        System.out.println("\nФормирование Карты Map");
        
        person1.id = 1L;
        person1.fistName = "Иван";
        person1.middleName = "Иванoвич";
        person1.sureName = "Иванoв";
        person1.birthDate = LocalDate.of(1992, Month.MARCH, 1);
        
        person2.id = 2L;
        person2.fistName = "Иван";
        person2.middleName = "Иванoвич";
        person2.sureName = "Иванoв";
        person2.birthDate = LocalDate.of(1992, Month.MARCH, 2);
        
        person3.id = 3L;
        person3.fistName = "Иван";
        person3.middleName = "Иванoвич";
        person3.sureName = "Иванoв";
        person3.birthDate = LocalDate.of(1992, Month.DECEMBER, 15);
        
        person4.id = 4L;
        person4.fistName = "Иван";
        person4.middleName = "Иванoвич";
        person4.sureName = "Иванoв";
        person4.birthDate = LocalDate.of(1992, Month.DECEMBER, 15);
        
      
        
        personMap.put(person1.id, person1);
        personMap.put(person2.id, person2);
        personMap.put(person3.id, person3);
        personMap.put(person4.id, person4);
        
        
        System.out.println("Набор: "+personMap);
    
    }
    
    
}
