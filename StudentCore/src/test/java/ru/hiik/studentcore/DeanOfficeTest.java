/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.hiik.studentcore;

import java.util.List;
import org.junit.Test;
import org.junit.Assert;

/**
 *
 * @author developer
 */

public class DeanOfficeTest {

    DeanOffice deanOffice = new DeanOffice();

    @Test ()
    public void testInstance() {
        System.out.println("Тестирование класса DeanOffice - создание экземпляра");
        Assert.assertNotNull("Экземляр класса DeanOffice не создан", deanOffice);
    }

    /**
     * Добавление пустого сотруника
     */
    @Test
    public void addEmptyEmployee() {
        System.out.println("Тестирование класса DeanOffice - добавление сотруника");
        Employee employee = new Employee();
        employee = null;
        boolean result = deanOffice.addEmpolyee(employee);
        Assert.assertFalse("Неверно выполнился метод addEmpolyee: ожидаемое значение False (сотрудник не добавлен), а полечено - TRUE", result);
        
    }
    
    
    @Test
    public void addEmployee() {
        System.out.println("Тестирование класса DeanOffice - добавление сотруника");
        Employee employee = new Employee();
        boolean result = deanOffice.addEmpolyee(employee);
        Assert.assertTrue("Пользователь не добавился", result);
    }
    
    
    
    @Test
    public void getEmployeeList() {
        System.out.println("Тестирование класса DeanOffice - получение списка сотрудников");
        List<Employee> employees = deanOffice.getEmployeeLits();
        System.out.println("Список сотруников: "+employees);
    }

}
