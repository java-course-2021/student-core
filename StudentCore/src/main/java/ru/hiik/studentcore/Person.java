/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.hiik.studentcore;

import java.time.LocalDate;
import java.util.Objects;

/**
 *
 * @author developer
 */
public class Person {
    
    public long  id;            //  Номер записи в БД персоны
    public String fistName;     //  Имя 
    public String sureName;     //  Фамилия
    public String middleName;   //  Отчество
    public LocalDate birthDate; // дата рождения

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.fistName);
        hash = 79 * hash + Objects.hashCode(this.sureName);
        hash = 79 * hash + Objects.hashCode(this.middleName);
        hash = 79 * hash + Objects.hashCode(this.birthDate);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Person other = (Person) obj;
        if (!Objects.equals(this.fistName, other.fistName)) {
            return false;
        }
        if (!Objects.equals(this.sureName, other.sureName)) {
            return false;
        }
        if (!Objects.equals(this.middleName, other.middleName)) {
            return false;
        }
        if (!Objects.equals(this.birthDate, other.birthDate)) {
            return false;
        }
        return true;
    }

   

   

    
    
    @Override
    public String toString( )
    {
        
        return "id ="+id +" "+sureName +" "+fistName+" "+middleName +" "+birthDate;
    }
    
    
}
