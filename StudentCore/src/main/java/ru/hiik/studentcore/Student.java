/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.hiik.studentcore;

import java.time.LocalDate;

/**
 *
 * @author developer
 */
public class Student extends Person {
    
  
    public int yearOfStudy;  // Курс 
    public String group;        // Курс
  
    
    public String toString()
    {
        return "Студент: "+super.toString() +" группа: "+group +" курс:"+yearOfStudy;
    }        
    
}
